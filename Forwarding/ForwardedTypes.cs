﻿using Forwarding;
using System.Runtime.CompilerServices;

[assembly:TypeForwardedTo(typeof(Foo))]